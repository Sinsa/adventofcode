//Change in input required for this to work: Turn the boxes by 90 degrees
//this file is mostly the same as task 1, changes were made in lines

const fs = require("fs");
const text = fs.readFileSync("./input.txt") + "";
const input = text.split("\r\n");

const boxStacks = [[]];

for (inputLine of input) {
    if (inputLine === "") {
        break;
    }
    let boxId = inputLine.match(/\d+/)[0] - 1;
    let boxes = inputLine.replace(/\W/g, '').split("");
    boxes.shift();
    boxStacks[boxId] = boxes;
}

console.log(boxStacks);

let workTime = false;
let j = 0;
for (const inputLine of input) {
    if (!workTime) {
        if (inputLine === "") {
            workTime = true;
        }
        continue;
    }
    console.log(`start of ${j}`);
    const tasks = inputLine.replace(/\D/g, '').split("");
    if (tasks.length > 3) {
        const factor10 = tasks.shift();
        tasks[0] = factor10 + tasks[0];
    }
    console.log(`move ${tasks[0]} from ${tasks[1]} to ${tasks[2]}`);
    let remainingBoxesToMove = tasks[0];
    const fromStack = tasks[1] - 1;
    const toStack = tasks[2] - 1;

    const craneArray = [];
    while (remainingBoxesToMove > 0) {
        craneArray.push(boxStacks[fromStack].pop());
        remainingBoxesToMove--;
    }
    craneArray.reverse();
    boxStacks[toStack].push(...craneArray);

    console.log(boxStacks);
    console.log(`end of ${j}`);
    j++;
}


for (const boxStack of boxStacks) {
    console.log(boxStack.join());
}

let topBoxes = "";
for (const boxStack of boxStacks) {
    topBoxes += boxStack.pop();
}
console.log(topBoxes)
