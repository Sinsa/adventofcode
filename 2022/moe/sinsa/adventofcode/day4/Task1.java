package moe.sinsa.adventofcode.day4;

import moe.sinsa.adventofcode.utility.InputReader;

import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        List<String> input = InputReader.readInput(4, false);
        int amountOfRedundantTasks = 0;
        for (String pair : input) {
            String[] pairTasks = pair.split(",");
            int elv1FirstTask = Integer.parseInt(pairTasks[0].substring(0, pairTasks[0].indexOf("-")));
            int elv1SecondTask = Integer.parseInt(pairTasks[0].substring(pairTasks[0].indexOf("-") + 1));
            int elv2FirstTask = Integer.parseInt(pairTasks[1].substring(0, pairTasks[1].indexOf("-")));
            int elv2SecondTask = Integer.parseInt(pairTasks[1].substring(pairTasks[1].indexOf("-") + 1));
            if ((elv1FirstTask <= elv2FirstTask && elv1SecondTask >= elv2SecondTask) || (elv2FirstTask <= elv1FirstTask && elv2SecondTask >= elv1SecondTask)) {
                amountOfRedundantTasks++;
            }
        }
        System.out.println(amountOfRedundantTasks);
    }
}
