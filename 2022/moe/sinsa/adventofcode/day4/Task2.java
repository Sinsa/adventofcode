package moe.sinsa.adventofcode.day4;

import moe.sinsa.adventofcode.utility.InputReader;

import java.util.List;

public class Task2 {

    public static void main(String[] args) {
        List<String> input = InputReader.readInput(4, false);
        int amountOfOverlapTasks = 0;
        for (String pair : input) {
            String[] pairTasks = pair.split(",");
            int elv1FirstTask = Integer.parseInt(pairTasks[0].substring(0, pairTasks[0].indexOf("-")));
            int elv1SecondTask = Integer.parseInt(pairTasks[0].substring(pairTasks[0].indexOf("-") + 1));
            int elv2FirstTask = Integer.parseInt(pairTasks[1].substring(0, pairTasks[1].indexOf("-")));
            int elv2SecondTask = Integer.parseInt(pairTasks[1].substring(pairTasks[1].indexOf("-") + 1));
            if (isBetweenOrEqual(elv1FirstTask, elv2FirstTask, elv2SecondTask)) {
                amountOfOverlapTasks++;
            } else if (isBetweenOrEqual(elv1SecondTask, elv2FirstTask, elv2SecondTask)) {
                amountOfOverlapTasks++;
            } else if (isBetweenOrEqual(elv2FirstTask, elv1FirstTask, elv1SecondTask)) {
                amountOfOverlapTasks++;
            } else if (isBetweenOrEqual(elv2SecondTask, elv1FirstTask, elv1SecondTask)) {
                amountOfOverlapTasks++;
            }
        }
        System.out.println(amountOfOverlapTasks);
    }

    private static boolean isBetweenOrEqual(int suspect, int top, int bottom) {
        return (bottom >= suspect && suspect >= top) || (top >= suspect && suspect >= bottom);
    }
}
