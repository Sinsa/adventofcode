module.exports = {
    getPacketStart(distinctPacketsLength) {
        const fs = require("fs");
        const text = fs.readFileSync("./input.txt") + "";

        const datastream = text.substring(distinctPacketsLength).split("")
        const datastreamLength = text.length;
        let markerFound = false;
        let lastMarkers = [...text.substring(0, distinctPacketsLength).split("")];
        let currentDatastreamPosition = distinctPacketsLength;

        while (!markerFound && currentDatastreamPosition < datastreamLength) {
            console.log(`Checking Datastream Position ${currentDatastreamPosition}...`);
            if (areThereDuplicates(lastMarkers)) {
                currentDatastreamPosition++;
                lastMarkers.shift();
                lastMarkers.push(datastream.shift());
            } else {
                markerFound = true;
                console.log('Found the beginning of the datastream!');
                console.log(`The Datastream starts at position ${currentDatastreamPosition} with the packets ${lastMarkers.join("")}!`);
                return currentDatastreamPosition;
            }
        }

        if (!markerFound) {
            console.warn('No beginning of datastream found! Exiting.');
            return null;
        } else {
            throw new Error("I have no clue what's happening but something fucked up.");
        }


        function areThereDuplicates(array) {
            return new Set(array).size !== array.length;
        }
    }
}