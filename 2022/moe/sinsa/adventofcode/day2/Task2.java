package moe.sinsa.adventofcode.day2;

import moe.sinsa.adventofcode.day2.models.RpsMatch;

import java.util.List;

public class Task2 {

    public static void main(String[] args) {
        List<RpsMatch> matchList = Shared.runTournament(2);
        int totalPoints = 0;
        for (RpsMatch match : matchList) {
            totalPoints += match.calculateScore();
        }
        System.out.println(totalPoints);
    }
}
