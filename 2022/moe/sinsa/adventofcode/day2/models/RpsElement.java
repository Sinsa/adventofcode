package moe.sinsa.adventofcode.day2.models;

public enum RpsElement {
    ROCK,
    PAPER,
    SCISSORS
}
