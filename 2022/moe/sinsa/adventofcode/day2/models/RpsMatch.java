package moe.sinsa.adventofcode.day2.models;

public class RpsMatch {
    private RpsMatchOutcome outcome;
    private final RpsElement ownChoice;
    private final RpsElement opponentChoice;

    public RpsMatch(RpsElement ownChoice, RpsElement opponentChoice) {
        this.ownChoice = ownChoice;
        this.opponentChoice = opponentChoice;
        setOutcome();
    }

    public int calculateScore() {
        int totalScore = 0;
        if (outcome == RpsMatchOutcome.WIN) {
            totalScore += 6;
        } else if (outcome == RpsMatchOutcome.DRAW) {
            totalScore += 3;
        }

        if (ownChoice == RpsElement.ROCK) {
            totalScore += 1;
        } else if (ownChoice == RpsElement.PAPER) {
            totalScore += 2;
        } else {
            totalScore += 3;
        }
        return totalScore;
    }

    private void setOutcome() {
        if (ownChoice == RpsElement.ROCK) {
            if (opponentChoice == RpsElement.PAPER) {
                outcome = RpsMatchOutcome.LOSS;
            } else if (opponentChoice == RpsElement.SCISSORS) {
                outcome = RpsMatchOutcome.WIN;
            } else {
                outcome = RpsMatchOutcome.DRAW;
            }
        } else if (ownChoice == RpsElement.PAPER) {
            if (opponentChoice == RpsElement.SCISSORS) {
                outcome = RpsMatchOutcome.LOSS;
            } else if (opponentChoice == RpsElement.ROCK) {
                outcome = RpsMatchOutcome.WIN;
            } else {
                outcome = RpsMatchOutcome.DRAW;
            }
        } else {
            if (opponentChoice == RpsElement.ROCK) {
                outcome = RpsMatchOutcome.LOSS;
            } else if (opponentChoice == RpsElement.PAPER) {
                outcome = RpsMatchOutcome.WIN;
            } else {
                outcome = RpsMatchOutcome.DRAW;
            }
        }
    }

}
