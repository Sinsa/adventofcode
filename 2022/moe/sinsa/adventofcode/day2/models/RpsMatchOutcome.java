package moe.sinsa.adventofcode.day2.models;

public enum RpsMatchOutcome {
    WIN,
    LOSS,
    DRAW
}
