package moe.sinsa.adventofcode.day2;

import moe.sinsa.adventofcode.day2.models.RpsElement;
import moe.sinsa.adventofcode.day2.models.RpsMatch;
import moe.sinsa.adventofcode.utility.InputReader;

import java.util.ArrayList;
import java.util.List;

public class Shared {

    public static List<RpsMatch> runTournament(int task) {
        List<String> input = InputReader.readInput(2);
        List<RpsMatch> matchList = new ArrayList<>();
        for (String matchStrategy : input) {
            char[] matchStrategyAsCharArray = matchStrategy.toCharArray();
            char opponentChoiceStrategy = matchStrategyAsCharArray[0];
            char ownChoiceStrategy = matchStrategyAsCharArray[2];
            RpsElement opponentChoice = getRpsElementFromPickStrategy(opponentChoiceStrategy);
            RpsElement ownChoice;
            if (task == 1) {
                ownChoice = getRpsElementFromPickStrategy(ownChoiceStrategy);
            } else {
                ownChoice = getRpsElementFromOutcomeStrategy(opponentChoice, ownChoiceStrategy);
            }
            RpsMatch currentMatch = new RpsMatch(ownChoice, opponentChoice);
            matchList.add(currentMatch);
        }
        return matchList;
    }

    private static RpsElement getRpsElementFromPickStrategy(char strategy) {
        if (strategy == 'A' || strategy == 'X') {
            return RpsElement.ROCK;
        } else if (strategy == 'B' || strategy == 'Y') {
            return RpsElement.PAPER;
        } else {
            return RpsElement.SCISSORS;
        }
    }

    // X = Loss
    // Y = Draw
    // Z = Win
    private static RpsElement getRpsElementFromOutcomeStrategy(RpsElement opponentElement, char strategy) {
        if (opponentElement == RpsElement.ROCK) {
            if (strategy == 'X') {
                return RpsElement.SCISSORS;
            } else if (strategy == 'Y') {
                return RpsElement.ROCK;
            } else {
                return RpsElement.PAPER;
            }
        } else if (opponentElement == RpsElement.PAPER) {
            if (strategy == 'X') {
                return RpsElement.ROCK;
            } else if (strategy == 'Y') {
                return RpsElement.PAPER;
            } else {
                return RpsElement.SCISSORS;
            }
        } else {
            if (strategy == 'X') {
                return RpsElement.PAPER;
            } else if (strategy == 'Y') {
                return RpsElement.SCISSORS;
            } else {
                return RpsElement.ROCK;
            }
        }
    }
}
