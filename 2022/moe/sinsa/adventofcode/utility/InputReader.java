package moe.sinsa.adventofcode.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputReader {

    public static List<String> readInput(int day, boolean fake) {
        String path;
        if (fake) {
            path = "E:\\Sinsa\\Backup\\Discord\\Projects\\adventofcode\\2022\\moe\\sinsa\\adventofcode\\day" + day + "\\fakeInput.txt";
        } else {
            path = "E:\\Sinsa\\Backup\\Discord\\Projects\\adventofcode\\2022\\moe\\sinsa\\adventofcode\\day" + day + "\\input.txt";
        }
        List<String> input = new ArrayList<>();
        try {
            InputStream inputStream = new FileInputStream(path);
            Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name());
            while (scanner.hasNextLine()) {
                input.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return input;
    }

    //default to real input
    public static List<String> readInput(int day) {
        return readInput(day, false);
    }
}
