package moe.sinsa.adventofcode.day1;

import java.util.List;

public class Task2 {

    public static void main(String[] args) {
        List<Integer> elves = Shared.getCaloriesPerElf();
        int top1elf = 0;
        int top2elf = 0;
        int top3elf = 0;
        for (int elf : elves) {
            if (elf > top3elf) {
                if (elf > top2elf) {
                    if (elf > top1elf) {
                        top3elf = top2elf;
                        top2elf = top1elf;
                        top1elf = elf;
                    } else {
                        top3elf = top2elf;
                        top2elf = elf;
                    }
                } else {
                    top3elf = elf;
                }
            }
        }
        System.out.println("Top 1 Elf:\t" + top1elf);
        System.out.println("Top 2 Elf:\t" + top2elf);
        System.out.println("Top 3 Elf:\t" + top3elf);
        System.out.println("Total Cal:\t" + (top1elf + top2elf + top3elf));
    }
}
