package moe.sinsa.adventofcode.day1;

import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        List<Integer> elves = Shared.getCaloriesPerElf();
        int highestAmountOfCalories = 0;
        for (int elf : elves) {
            if (elf > highestAmountOfCalories) {
                highestAmountOfCalories = elf;
            }
        }
        System.out.println(highestAmountOfCalories);
    }
}
