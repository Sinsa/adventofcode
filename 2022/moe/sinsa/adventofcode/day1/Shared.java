package moe.sinsa.adventofcode.day1;

import moe.sinsa.adventofcode.utility.InputReader;

import java.util.ArrayList;
import java.util.List;

public class Shared {
    public static List<Integer> getCaloriesPerElf() {
        List<String> input = InputReader.readInput(1);
        List<Integer> elves = new ArrayList<>();
        int currentCalories = 0;
        for (String singleInputLine : input) {
            if (singleInputLine.length() == 0) {
                elves.add(currentCalories);
                currentCalories = 0;
            } else {
                int calories = Integer.parseInt(singleInputLine);
                currentCalories += calories;
            }
        }
        return elves;
    }
}
