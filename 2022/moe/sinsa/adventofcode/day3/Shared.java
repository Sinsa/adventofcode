package moe.sinsa.adventofcode.day3;

import moe.sinsa.adventofcode.utility.InputReader;

import java.util.ArrayList;
import java.util.List;

public class Shared {

    public static List<Character> unloadBackpacks() {
        List<String> input = InputReader.readInput(3, false);
        List<Character> sharedItemTypes = new ArrayList<>();
        for (String singleBackpack : input) {
            int mid = singleBackpack.length() / 2;
            char[] compartment1 = singleBackpack.substring(0, mid).toCharArray();
            char[] compartment2 = singleBackpack.substring(mid).toCharArray();
            char sharedItemType = 0;
            for (char itemInCompartment1 : compartment1) {
                for (char itemInCompartment2 : compartment2) {
                    if (itemInCompartment1 == itemInCompartment2) {
                        sharedItemType = itemInCompartment1;
                        break;
                    }
                }
            }
            sharedItemTypes.add(sharedItemType);
        }
        return sharedItemTypes;
    }

    public static int convertItemToPriority(char itemType) {
        if (Character.isUpperCase(itemType)) {
            itemType = (char) (Character.toLowerCase(itemType) - 6);
        } else {
            itemType = Character.toUpperCase(itemType);
        }
        return itemType - 64;
    }
}
