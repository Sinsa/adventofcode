package moe.sinsa.adventofcode.day3;

import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        List<Character> sharedItemTypes = Shared.unloadBackpacks();
        int totalPriorities = 0;
        for (char sharedItemType : sharedItemTypes) {
            totalPriorities += Shared.convertItemToPriority(sharedItemType);
        }
        System.out.println(totalPriorities);
    }
}
