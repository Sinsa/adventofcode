package moe.sinsa.adventofcode.day3;

import moe.sinsa.adventofcode.utility.InputReader;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Task2 {
    public static void main(String[] args) {
        List<String> backpacks = InputReader.readInput(3, false);
        int totalPriority = 0;
        for (int i = 0; i < backpacks.size() / 3; i++) {
            int backpack1Index = i * 3;
            String backpack1 = backpacks.get(backpack1Index);
            String backpack2 = backpacks.get(backpack1Index + 1);
            String backpack3 = backpacks.get(backpack1Index + 2);
            String possibleItemTypes = backpack1;
            for (char itemType : backpack1.toCharArray()) {
                if (!backpack2.contains(Character.toString(itemType)) || !backpack3.contains(Character.toString(itemType))) {
                    possibleItemTypes = possibleItemTypes.replaceAll(Character.toString(itemType), "");
                }
            }
            if (possibleItemTypes.length() != 1) {
                char[] arr = possibleItemTypes.toCharArray();
                Set<Character> set = new HashSet<>();
                for (char c : arr) {
                    set.add(c);
                }
                if (set.size() != 1) {
                    throw new RuntimeException("Group " + i * 3 + " has not exactly 1 item type in common!\n" + backpack1 + "\n" + backpack2 + "\n" + backpack3 + "\n\n" + possibleItemTypes);
                }
            }
            totalPriority += Shared.convertItemToPriority(possibleItemTypes.toCharArray()[0]);
        }
        System.out.println(totalPriority);
    }

}
