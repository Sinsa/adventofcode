import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day3Task2 {
    public static void main(String[] args) throws Exception {
        List<String> diagnosticReport = InputReader.readJson(3);
        String oxygenGeneratorRating;
        String co2ScrubberRating;
        // generate oxygen generator rating
        List<String> oxygenGeneratorReports = diagnosticReport;
        for (int i = 0; oxygenGeneratorReports.size()>1; i++) {
            int count0 = 0;
            int count1 = 0;
            for (String measurement : oxygenGeneratorReports) {
                if (measurement.toCharArray()[i] == '0') {
                    count0++;
                } else {
                    count1++;
                }
            }
            if (count0 > count1) {
                int finalI = i;
                oxygenGeneratorReports = oxygenGeneratorReports.stream().filter(singleReport -> singleReport.toCharArray()[finalI]=='0').collect(Collectors.toList());
            } else {
                int finalI1 = i;
                oxygenGeneratorReports = oxygenGeneratorReports.stream().filter(singleReport -> singleReport.toCharArray()[finalI1]=='1').collect(Collectors.toList());
            }

        }
        oxygenGeneratorRating = oxygenGeneratorReports.get(0);

        // generate co2 scrubber rating
        List<String> co2ScrubberReports = diagnosticReport;
        for (int i = 0; co2ScrubberReports.size()>1; i++) {
            int count0 = 0;
            int count1 = 0;
            for (String measurement : co2ScrubberReports) {
                if (measurement.toCharArray()[i] == '0') {
                    count0++;
                } else {
                    count1++;
                }
            }
            if (count0 <= count1) {
                int finalI = i;
                co2ScrubberReports = co2ScrubberReports.stream().filter(singleReport -> singleReport.toCharArray()[finalI]=='0').collect(Collectors.toList());
            } else {
                int finalI1 = i;
                co2ScrubberReports = co2ScrubberReports.stream().filter(singleReport -> singleReport.toCharArray()[finalI1]=='1').collect(Collectors.toList());
            }
            System.out.println("new print block");
            System.out.println("0:\t"+count0+"\n1:\t"+count1);
            co2ScrubberReports.forEach(System.out::println);
        }
        co2ScrubberRating = co2ScrubberReports.get(0);

        System.out.println("Pure oxygen: " + oxygenGeneratorRating);
        System.out.println("Pure Scrubber: " + co2ScrubberRating);
        System.out.println("Oxygen Rate: " + Integer.parseInt(oxygenGeneratorRating, 2));
        System.out.println("Scrubber Rate: " + Integer.parseInt(co2ScrubberRating, 2));
        System.out.println("Power consumption: " + Integer.parseInt(oxygenGeneratorRating, 2) * Integer.parseInt(co2ScrubberRating, 2));
    }
}
