import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class InputReader {
    /**
     * This static method gets the input from the input.json for the queried day
     * @param day The day the input is requested from
     * @return the input as a String Array
     * @throws Exception can be thrown when the inputs file is missing or not found
     */
    public static List<String> readJson(int day) throws Exception {
        Object object = new JSONParser().parse(new FileReader("2021/inputs.json"));
        JSONObject inputs = (JSONObject) object;
        JSONArray days = (JSONArray) inputs.get("days");
        JSONObject singleDay = (JSONObject) days.get(day - 1);
        JSONArray singleDayInput = (JSONArray) singleDay.get("input");
        List<String> usableList = new ArrayList<>();
        for (Object o : singleDayInput) {
            usableList.add(o.toString());
        }
        return usableList;
    }
}
