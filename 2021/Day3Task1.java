import java.util.List;

public class Day3Task1 {
    public static void main(String[] args) throws Exception {
        List<String> diagnosticReport = InputReader.readJson(3);
        StringBuilder epsilonRate = new StringBuilder();
        StringBuilder gammaRate = new StringBuilder();
        //todo fix after fixing
        for (int i = 0; i < 12; i++) {
            int count0 = 0;
            int count1 = 0;
            for (String measurement : diagnosticReport) {
                System.out.println(measurement + " : " + i + " = " + measurement.toCharArray()[i]);
                if (measurement.toCharArray()[i] == '0') {
                    count0++;
                } else {
                    count1++;
                }
            }
            System.out.println("0:\t" + count0 + "\n1:\t" + count1);
            if (count0 > count1) {
                epsilonRate.append(1);
                gammaRate.append(0);
            } else {
                epsilonRate.append(0);
                gammaRate.append(1);
            }
        }
        System.out.println("Pure Epsilon: " + epsilonRate.toString());
        System.out.println("Pure Gamma: " + gammaRate.toString());
        System.out.println("Epsilon Rate: " + Integer.parseInt(epsilonRate.toString(), 2));
        System.out.println("Gamma Rate: " + Integer.parseInt(gammaRate.toString(), 2));
        System.out.println("Power consumption: " + Integer.parseInt(epsilonRate.toString(), 2) * Integer.parseInt(gammaRate.toString(), 2));
//        radix has to be 2 since we're using binary here
//        Integer.parseInt("1001",2);
    }
}
