const reports = require("./inputs.json").days[0].input;

let higherthan=0;

for(let i=0;i<reports.length-3;i++) {
    if((reports[i]+reports[i+1]+reports[i+2])<(reports[i+1]+reports[i+2]+reports[i+3])) {
        higherthan++;
    }
}

console.log("final result: "+higherthan);